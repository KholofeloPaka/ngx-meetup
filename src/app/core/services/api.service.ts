import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private host: string;
  private apiKey: string;
  private proxy: string;

  /**
   * Creating an instance of ApiService
   * @param {HttpClient} http   The htpp service being injected
   * @param {ConfigService} configService   The config service being injected
   */
  constructor(
    private http: HttpClient,
    private configService: ConfigService) {
      this.host = this.configService.get('host');
      this.apiKey = this.configService.get('host_api_key');
      this.proxy = this.configService.get('proxy');
  }

  /**
   *  Get data from the api
   * @param {string} method   The method that to be called in the api
   *
   * @return {Observable<*>}  The observable data model return from the api
   */
  public get(method: string, param: Object = {}): Observable<any> {
    let url: string = this.constructUrl(method);
    let params: HttpParams = this.constructParams(param);
    return this.http.get(url, {params: params});
  }

  /**
   * Create a url structure by concatinating proxy, host and method
   * @param {string} method   The method that to be called in the api
   *
   * @return {string}  The fully constrcted url
   */
  private constructUrl(method: string): string {
    return this.proxy + this.host + method;
  }

  /**
   * Create http params from an object
   * @param {Object} param   The object that will be converted into params
   *
   *  @return {HttpParams}  The fully constrcted params
   */
  private constructParams(param: Object): HttpParams {
    let params = new HttpParams();
    if (this.apiKey) {
      params = params.append('key', this.apiKey);
    }

    for (var key in param) {
      if (param.hasOwnProperty(key)) {
        let val = param[key];
        params = params.append(key, val);
      }
    }

    return params;
  }
}
