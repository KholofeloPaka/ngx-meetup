import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
    private configData: any;

    /**
     * Create an instance of config service
     * @param {HttpClient} http   The htpp service being injected
     */
    constructor(private http: HttpClient) {

    }

    /**
     * Load data from a config json
     * @param {string} path   The path to load a config json from
     * @return {*}
     */
    public load(path = './assets/config.json'): Promise<void> {
        return this.http.get(path)
            .toPromise()
            .then(config => {
                this.configData = config;
            }).catch(err => {

            });
    }

    /**
     * Get config data by key
     * @param {string} string   The key being used to retrive config data
     * @return mixed configData;
     */
    public get(key: string): any {
        return this.configData[key];
    }

}
