import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

/**
 * This sevesrity levels are defined by syslog at https://tools.ietf.org/html/rfc5424
 */
enum SEVERITY_LEVEL {
  ERROR = 0,
  WARN = 4,
  INFO = 6,
  DEBUG = 7
}

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  private logs: Array<any> = [];
  private interval: number;
  private lastError: any;

  /**
   * Create an instance of LoggerService.
   * @param {HttpClient} HttpClient     The HttpClient Service being injected.
   */
  constructor(private http: HttpClient
  ) {
    //TODO: Inject config service and specify the logging host url
    //TODO: Inject api service and use it for http calls
     this.interval = 5000;
     setTimeout(this.sendToServer(), this.interval);
  }

  /**
   * Outputs an info message.
   * @param {*} message          The message being logged.
   * @return void
   */
  public info(...message): void {
    this.addLog(SEVERITY_LEVEL.INFO, message);
  }

  /**
   * Outputs an warning message.
   * @param {*} message          The message being logged.
   * @return void
   */
  public warn(...message): void {
    this.addLog(SEVERITY_LEVEL.WARN, message);
  }

  /**
   * Outputs an error message.
   * @param {*} message  The message being logged.
   * @return void
   */
  public error(...message): void {
    this.addLog(SEVERITY_LEVEL.ERROR, message);
  }

  /**
   * Outputs a debug message.
   * @param {*} message  The message being logged.
   * @return void
   */
  public debug(...message): void {
    this.addLog(SEVERITY_LEVEL.DEBUG, message);
  }

  /**
   * Add log messages to a log variable.
   * @param {*} message      The message being logged.
   * @param {number} severity     The severity level of the message being logged.
   * @return void
   */
  private addLog(severity: number, message): void {
    // Check if we don't store same error to the logs
    if (this.lastError !== message) {
      this.logs.push('Sererity Level:' + severity + '->' + message);
      this.lastError = message;
    }
  }

  /**
   * Send logs to a server.
   * @return void
   */
  private sendToServer(): void {
    if (this.logs.length > 0) {
      //TODO: Get the url from config service
       let logPath = "tt";
      if ( typeof logPath !== 'undefined' ) {
      // this.http.get(logPath, this.logs); Uncomment when you have an api service for stashing logs
      }
      this.logs = [];
    }
    console.log("sendToServer");
  }
}
