import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CoreModule } from './core';
import { SharedModule,  HeaderComponent, FooterComponent } from './shared';
import { AccountModule } from './account/account.module';
import { AppRoutingModule } from './app-routing.module';
import { GroupModule } from './group/group.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CoreModule,
    SharedModule,
    AppRoutingModule,
    AccountModule,
  ],
  providers:[],
  bootstrap: [AppComponent]
})
export class AppModule { }
