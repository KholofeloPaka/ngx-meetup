import { NgModule } from '@angular/core';
import { PreloadAllModules, Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

const appRoutes: Routes = [
  {
    path: 'settings',
    loadChildren: './account/account.module#AccountModule'
  },
  {
    path: 'groups',
    loadChildren: './group/group.module#GroupModule'
  },
  {
    path: '',
    loadChildren: './group/group.module#GroupModule'
  }
];


@NgModule({
  imports: [CommonModule, RouterModule.forRoot(appRoutes,  { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }