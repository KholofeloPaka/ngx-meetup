import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";

import { LoggerService } from '../../core';
import { CategoryService } from '../../services';
import { Category } from '../../models';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  private categories: Array<Category>;
  private profileForm: FormGroup;

  /**
   * Create instance of SettingsComponent
   * @param {CategoryService} categoryService   The category service being injected
   * @param {FormBuilder} formBuilder   The form builder service being injected
   * @param {Router} router   The router service being injected
   * @param {LoggerService} loggerService   The logger service being injected
   */
  constructor(
    private categoryService: CategoryService,
    private fb: FormBuilder,
    private router: Router,
    private loggerService: LoggerService
  ) { }

  ngOnInit() {
    this.getCategories();

    this.profileForm = this.fb.group({category:['']});
  }

  public onSubmit(): void {
    let selectedCategory: string = this.profileForm.value["category"];
    let category: Category = new Category({"id": selectedCategory});

    this.categoryService.saveCategoryPreference(category);
    this.router.navigate(["/groups"]);
  }

  /**
   * Initate a service call to get categories
   */
  private getCategories(): void {
    this.categoryService.getCategories().subscribe((categories: Array<Category>) => {
        this.categories = categories;
      }, (error) => {
        this.loggerService.error('Get Categoeries', error);
      },() => {
        this.loggerService.info('Get Categoeries', this.categories);
      });
  }

}
