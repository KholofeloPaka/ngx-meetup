import { Injectable } from "@angular/core";
import { HttpParams } from '@angular/common/http';

import { map } from "rxjs/operators";
import { Observable } from 'rxjs';

import { ApiService, ConfigService } from '../core';
import { CategoryService } from './category.service';

import { Group } from '../models';
import { Category, GroupsFilters } from "../models";

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  private searchCategories: string;
  private prefferedCategory: string;

  /**
   * Creating an instance of GroupService
   * @param {ApiService} http   The htpp service being injected
   * @param {ConfigService} configService   The config service being injected
   * @param {CategoryService} categoryService   The config service being injected
   */
  constructor(
     private apiService: ApiService,
     private configService: ConfigService,
     private categoryService: CategoryService
    ) {

  }

  /**
   * Get meetup group for certain categories
   * @param {Category} Category   The category to filter our groups with
   *
   * @return {Observable<Array<Group>}
   */
  public getGroups(categories: string = ''): Observable<Array<Group>> {
    let method: string = this.configService.get('meet_up_groups');
    const params = {
      country: "ZA",
      location: "Johannesburg",
      category: categories,
      page:"40"
    };

    let groupsFilters = new GroupsFilters();
    groupsFilters.category = categories;
    groupsFilters.location = "Johannesburg";
    console.log(groupsFilters.toProperties());
    return this.apiService.get(method, params)
      .pipe(
        map(json => {
          let groups: Array<Group> = [];
          json.forEach((group: Array<Group>) => {
            let groupObject: Group = new Group(group);
            groups.push(groupObject);
          });
          return groups;
        }
      ));
  }
}
