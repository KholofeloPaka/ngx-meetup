import { Injectable } from '@angular/core';

import { map } from "rxjs/operators";
import { Observable } from 'rxjs';

import { ApiService, ConfigService } from '../core';
import { Category } from "../models/category";
import { LocalStorage } from '@ngx-pwa/local-storage';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  /**
   * Creating an instance of GroupService
   * @param {ApiService} http   The htpp service being injected
   * @param {ConfigService} configService   The config service being injected
   * @param {LocalStorage} localStorage   The local storage service being injected
   */
  constructor(
     private apiService: ApiService,
     private configService: ConfigService,
     private localStorage: LocalStorage
    ) {
  }

  /**
   * Get meetup group for certain categories
   *
   * @return {Observable<Array<Group>}
   */
  public getCategories(): Observable<Array<Category>> {
    let method: string = this.configService.get('meet_up_category');
    const params = {
      page:"40"
    };
    return this.apiService.get(method, params)
      .pipe(
        map(json => {
          const categories: Array<Category> = [];
          const categoryJson = json["results"];
          categoryJson.forEach((category: Array<Category>) => {
            const categoryObject: Category = new Category(category);
            categories.push(categoryObject);
          });
          return categories;
        }
      ));
  }

  /**
   * Save selected category into a local storage
   * @param {Category} category being saved into local storage
   */
  public saveCategoryPreference(category: Category) {
    this.localStorage.setItem('category', category).subscribe(() => {});
  }

  /**
   * Get category prefference
   *
   */
  public getPrefference() {
    return this.localStorage.getItem('category');
  }
}
