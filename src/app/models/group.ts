import { GroupPhoto } from './group_photo';

export class Group {
  public id: string;
  public name: string;
  public link: string;
  public groupPhoto: GroupPhoto;

  constructor(json?: any) {
    if (json) {
       this.id = json.id;
       this.name = json.name;
       this.link = json.link;
       this.groupPhoto = new GroupPhoto(json.group_photo);
    } else {
      this.id = '';
      this.name = '';
      this.link = '';
      this.groupPhoto = new GroupPhoto();
    }
  }

}
