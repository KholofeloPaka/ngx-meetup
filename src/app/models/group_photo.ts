export class GroupPhoto {

    public baseUrl: string;
    public highresLink: string;
    public thumbLink: string;

    constructor(json?: any) {

    if (json) {
      this.baseUrl = json.base_url;
      this.highresLink = json.highres_link;
      this.thumbLink = json.thumb_link;
    } else {
      this.baseUrl = "";
      this.highresLink = "https://placehold.it/150x80?text=IMAGE";
      this.thumbLink = "https://placehold.it/150x80?text=IMAGE";
    }

}
}
