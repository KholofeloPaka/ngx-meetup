import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent, FooterComponent } from './layout';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    // FooterComponent, HeaderComponent
  ],
  exports: [
    // FooterComponent, HeaderComponent
  ]
})
export class SharedModule { }
