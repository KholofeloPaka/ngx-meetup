import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { GroupListComponent } from './group-list/group-list.component';
import { GroupRoutingModule } from './/group-routing.module';


@NgModule({
  imports: [
    CommonModule,
    GroupRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ GroupListComponent]
})
export class GroupModule { }
