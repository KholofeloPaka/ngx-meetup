import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { GroupListComponent } from './group-list/group-list.component';

const appRoutes: Routes = [
  {
    path: '',
    component: GroupListComponent,
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class GroupRoutingModule { }
