import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Router } from "@angular/router";

import { GroupService, CategoryService } from '../../services';
import { Category, Group } from '../../models';
import { LoggerService } from '../../core';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css']
})
export class GroupListComponent implements OnInit {

  private categoryId: Category;
  private prefferedCategory: string;
  private categories: Array<Category>;
  private groups: Array<Group>;
  private categoryForm: FormGroup;

  /**
   * Create instance of GroupListComponent
   * @param {CategoryService} categoryService   The category service being injected
   * @param {GroupService} groupService   The group service being injected
   * @param {FormBuilder} formBuilder   The form builder service being injected
   * @param {router} Router   The router service being injected
   */
  constructor(
    private groupService: GroupService,
    private categoryService: CategoryService,
    private fb: FormBuilder,
    private router: Router,
    private loggerService: LoggerService
  ) { }

  ngOnInit() {
    this.getPrefferedByCategory();
    this.getCategories();

    this.categoryForm = this.fb.group({category:[0]});
  }

  /**
   * Concatinate selected value with preffered category
   * @param {string} value The selected category id being concatinated to preffered category
   */
  public onChange(value: number): void {
    if (value) {
      let searchCategories: string = value  + "," + this.prefferedCategory;
      this.getGroups(searchCategories);
    }
  }

  /**
   * Initate a service call to get categories
   * @param {string} categoryIds category id's separated by a comma
   */
  private getCategories(): void {
    this.categoryService.getCategories().subscribe((categories: Array<Category>) => {
      this.categories = categories;
    }, error => {
      this.loggerService.error('Get Categoeries', error);
    },
    () => {
      this.loggerService.info('Get Categoeries', this.categories);
    });
  }

  /**
   * Initate a service call to get groups
   * @param {string} categoryIds category id's separated by a comma
   */
  private getGroups(categoryIds? :string): void {
    this.groupService.getGroups(categoryIds).subscribe((groups: Array<Group>) => {
      this.groups = groups;
    }, error => {
      this.loggerService.error('Get Groups', error);
    }, () => {
      this.loggerService.info('Get Groups', this.categories);
    });
  }

  /**
   * Initate a service call to get meet up groups
   */
  private getPrefferedByCategory(): void {
    this.categoryService.getPrefference().subscribe((category: Category) => {
      // Making sure that users select preffered category
      if (category.id == 'undefined') {
        this.router.navigate(["/settings"]);
      }
      this.prefferedCategory = category.id;
    }, error => {
      this.loggerService.error('Get Preffered category', error);
    }, () => {
      this.loggerService.info('Get Preffered category', 'Return category Id: ' + this.prefferedCategory);
      this.getGroups(this.prefferedCategory);
    });
  }

}
